﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace People
{
    [TestClass]
    public class TestPerson
    {
        private TestPerson testPerson;
        private Person driver;
        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            testPerson = new TestPerson();
            DateTime birthday = new DateTime(2001, 06, 25); //Instancing the birthday
            driver = new Person("Nathan","Rayburn",birthday);
        }

        [TestMethod]
        public void TestAge()
        {

            //given

            //TODO
            /** 
             * write the context of the functionality
             * variables with expected values and variables for calculated values
             * We need :
             * string Name
             * string Lastname
             * DateTime Date of birth
             * string of all 3 data
             * --------------------
             * Expected string Name
             * Expected string Lastname
             * Expected DateTime Date of birth
             * Expected string of all 3 expected data
             * Don't need to create all 3 expected variables ? 
             * 
             * **/
            //Calculated --------------------

            string name;
            string lastName;
            string calcString; //This string will be compared with the expected string
            int age; //This age wil be calculated
            string expectedName = "Nathan";
            string expectedLastname = "Rayburn";
            int years = 18;
            string expectedString = expectedName + ";" + expectedLastname + ";" + years; //expected string 
            //Creating a student 
            //then

            //TODO
            
            /**
             * retrieve caclulated data from 3 accessors
             * turn it into one string of the 3 data
             * **/
            //Retrieving the proprieties of the student by calculation

            name = driver.Name;
            lastName = driver.LastName;
            age = driver.Age; //Calculating the age

            calcString = name + ";" + lastName + ";" + age; //Creating the calculated string

            //when

            //TODO
            /**
             * test the expected string with the calculated string
             * 
             * **/

            Assert.AreEqual(expectedString, calcString); // Test the strings
            
        }
        [TestMethod]
        public void EnteringVehicle_BasicTest_Success()
        {
            /**
             * We need to be able to test if a person can drive.
             * By doing that we need to test his age.
             * We need a Class Driving for a certain vehicle to calculate the age minimum 
             * Expected : Age of the person , Age minimum for driving 
             * Calculated : Age minimum for driving and maybe calculate the age of the person ? 
             * 
             * **/

            //GIVEN
            int ExpectedStatus = 1; //  = driving
            int status;
            Vehicle vehicle = new Vehicle(driver);



            //THEN
            /**
             * Calculate the status after entering vehicle
             * **/
            vehicle.EnterVehcle();
            status = vehicle.Status;
    

            //WHEN


            Assert.AreEqual(ExpectedStatus, status);

        }

        [TestMethod]
        public void LeaveVehicle_BasicTest_Success()
        {
            /**
             * Test if the vehicle is empy
             * 
             * **/

            //GIVEN
            int expectedNumberOfPeople = 0; //  = driving
            int status;
            Vehicle car = new Vehicle(driver);



            //THEN
            /**
             * Calculate the status after leaving the vehicle
             * **/
            car.LeaveVehicle();
            status = car.Status;

            //WHEN


            Assert.AreEqual(expectedNumberOfPeople, status);
        }
        [TestMethod]
        public void Disponibility_AfterCreation_Success()
        {
            /**
             * Testing if the vehicle is avalable after creating a vehicle
             * **/

            //GIVEN

            bool status;
            Vehicle car = new Vehicle(driver);



            //THEN
            /**
             * Calculate the avalablity
             * **/
            status = car.Disponibility;

            //WHEN


            Assert.IsTrue(status);
        }
        [TestMethod]
        public void Disponibility_AfterOccupied_Success()
        {
            /**
             * Test the avalability when the car is occupied
             * 
             * **/

            //GIVEN
            int  numberOfPeopleInVehicle = 1; //  = driving
            bool ExpectedDispo = false;
            bool calcStatus;
            Vehicle car = new Vehicle(driver);



            //THEN
            /**
             * Calculate the status
             * **/
            car.Status = numberOfPeopleInVehicle;
            car.EnterVehcle();
            calcStatus = car.Disponibility;

            //WHEN
            
            /**
             * test age with expected age
             * 
             * **/

            Assert.AreEqual(ExpectedDispo, calcStatus);
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {


        }
    }
}
