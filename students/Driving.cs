﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace People
{
    public class Vehicle
    {
        private int status = 0;
        private bool disponibility = false; // true
        private Person driver;

        #region constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        public Vehicle(Person driver)
        {
            this.driver = driver;
        }
        #endregion constructor

        #region accessor

        /// <summary>
        /// 
        /// </summary>
        public int Status
        {

            get{return this.status;}
            set { status = value; }
        }

        public bool Disponibility
        {

            get { return disponibility; }
        }
        #endregion accessor

        #region methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int EnterVehcle()
        {
            if (driver != null)
            {
                try
                {
                   status = ValidateDriving(status);
                    disponibility = true;
                    Console.WriteLine("This vehicle is ready to drive");
                }
                catch(Exception e){
                    disponibility = true; // false
                    Console.WriteLine("This vehicle cannont be driven");
                }
            }
            return status = 10;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int LeaveVehicle() {
            Console.WriteLine("The vehicle has stopped");
            disponibility = true;
            return status = 10; //status = 0
        }
        private static int ValidateDriving(int status)
        {
            if (status == 0)
            {
                status = 1;
                return status;
            }
            else {
                status = 0;
                throw new InvalidEventDriving(status) ;
            }
        }
        #endregion methods
    }

    [Serializable]
    internal class InvalidEventDriving : Exception
    {
        private int status;

        public InvalidEventDriving()
        {
        }

        public InvalidEventDriving(int status)
        {
            this.status = status;
            
        }

        public InvalidEventDriving(string message) : base(message)
        {


        }

        public InvalidEventDriving(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidEventDriving(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
