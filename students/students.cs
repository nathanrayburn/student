﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace People
{
    public class Person
    {

        #region attributes
        //Students proprieties
        private string name; 
        private string lastName;
        private DateTime birth; //variable DateTime birth is a birthday date
        private bool driverLicense;


        #endregion attributes

        #region methods
        #endregion methods

        #region constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="lastName"></param>
        /// <param name="birthday"></param>
        public Person(string name,string lastName,DateTime birthday)
        {
            //Set the recieved values to the local variables
            this.name = name; 
            this.lastName = lastName;
            this.birth = birthday;

        }
        public Person(string name, string lastName, DateTime birthday,bool driverLicense)
        {
            //Set the recieved values to the local variables
            this.name = name;
            this.lastName = lastName;
            this.birth = birthday;
            this.driverLicense = driverLicense;
        }
        #endregion constructor

        #region accessor 
        /// <summary>
        /// Accessor string Name
        /// Set None
        /// get return int name
        /// Description : This function returns the value of the name
        /// </summary>
        public string Name

        {
            get { return name; }

        }

       /// <summary>
       /// Accessor string LastName
       /// Set None
       /// get return int lastName
       /// Description : This function returns the value of the lastname
       /// </summary>
        public string LastName
        {

            get { return lastName; }
        }
        #endregion accessor

        /// <summary>
        /// Accessor Age
        /// Set None
        /// Get return int years 
        /// Description : Calculates the age accurately By using the Years,months and days
        /// 
        /// </summary>
        public int Age
        {

            get {

                int years = DateTime.Now.Year - birth.Year; //Calculate the birday in years

                if ((birth.Month > DateTime.Now.Month) || (birth.Month == DateTime.Now.Month && birth.Day > DateTime.Now.Day)) //calculate the age more accurate with the months and days
                    years--;

                return years; //Returns the age
            }
        }


    }
}
